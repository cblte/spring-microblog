# Welcome to Spring Microblog

This is an example application featured in my Spring-Tutorial which I also used to learn the Spring Framework. See the tutorial for instructions on how to work with it.

This tutorial is based and heaviliy inspired by the Mega Flask Tutorial from Miguel Grindberg which I used to learn about the Flask Python Framework. You can find here the [Mega Flask Tutorial](http://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world) and [here the code repository](https://github.com/miguelgrinberg/microblog) for all the code related ot the Mega Flask Tutorial.
